﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour
{
     private Player mPlayer;
    [SerializeField] private bool facingRight = true;
    public Animator animator;
    [SerializeField] private bool mGrounded;
    private Rigidbody2D rb;
    [SerializeField] private Transform GroundCheck;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        mPlayer = GetComponent<Player>();
    }

    public void MoveUnit(Vector3 direction)
    {
        if (direction.x < 0 && facingRight)
        {
            flip();
        }
        else if (direction.x > 0 && !facingRight)
        {
            flip();
        }

        this.transform.position += direction * Time.deltaTime * mPlayer.SPD;
    }

    public void Jump(Vector3 direction)
    {
        rb.AddForce(direction * (mPlayer.SPD * 5));
    }

    void flip()
    {
        facingRight = !facingRight;

        transform.Rotate(0f, 180f, 0f);
    }


    void Update()
    {
        Vector3 dwn = transform.TransformDirection(Vector3.down);
        mGrounded = false;
        if (Physics2D.Raycast(GroundCheck.transform.position, dwn, 1))
        { 
            mGrounded = true;
        }

        float x = CrossPlatformInputManager.GetAxis("Horizontal");
        if (x != 0)
        {
            MoveUnit(new Vector3(x, 0, 0));
        }
        animator.SetFloat("Speed", Mathf.Abs(x));

        float y = CrossPlatformInputManager.GetAxis("Vertical");
        if (y > 0)
        {
            if(mGrounded == false){
                return;
            }
            Jump(new Vector3(0, y, 0));
            
        }
        animator.SetFloat("AirSpeed", Mathf.Abs(y));
    }

}
