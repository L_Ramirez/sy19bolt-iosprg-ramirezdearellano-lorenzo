﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading;
using UnityEngine;

public abstract class UnitStats : MonoBehaviour
{
    public int MaximumHP;
    public int ATK;
    public float SPD;
    public int DEF;
    [SerializeField]
    protected float currentHP;

    public bool isBurning = false;
    private bool facingRight = true;
    private Rigidbody2D rb;

    public Transform AttackArea;
    public float AttackRange;
    public LayerMask EnemyLayer;
    public Animator Animator;

    protected virtual void Start()
    {
        currentHP = MaximumHP;
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {

    }


    public void MoveUnit(Vector3 direction)
    {
        if(direction.x < 0 && facingRight)
        {
            flip();
        }
        else if (direction.x > 0 && !facingRight)
        {
            flip();
        }
        if (Animator != null) Animator.SetFloat("Speed", Mathf.Abs(direction.x));
        this.transform.position += direction * Time.deltaTime * SPD;
    }


    public void Jump(Vector3 direction)
    {
        rb.AddForce(direction * (SPD+10));
    }


    public void TakeDamage(int damageRecieved)
    {
        if ((damageRecieved - DEF) < 1)
        {
            currentHP -= 1;

        }
        else
        {
            currentHP -= (damageRecieved - DEF);
        }

    }

    public void setBurning(bool burningCheck)
    {
        isBurning = burningCheck;
        if (isBurning == true)
        {
            //turn on burn effect;
        }
    }

    public virtual void Attack()
    {
        if (Animator != null) Animator.SetTrigger("Attack");
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(AttackArea.position, AttackRange, EnemyLayer);

        foreach(Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<UnitStats>().TakeDamage(ATK);
            UnityEngine.Debug.Log("Hit");
        }
    }


    public void StatusChange(int changeATK, float changeSPD, int changeDEF) {
        ATK += changeATK;
        SPD += changeSPD;
        DEF += changeDEF;
    }

    public abstract void Die();

    void flip()
    {
        facingRight = !facingRight;

        transform.Rotate(0f, 180f, 0f);
    }

    void OnDrawGizmosSelected()
    {
        if (AttackArea == null)
            return;

        Gizmos.DrawWireSphere(AttackArea.position, AttackRange);
    }




}
