﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class Player : UnitStats

{
    public bool isCharmed = false;
    public bool isSlept = false;
    public Healthbar healthbar;
    private GameManager gameManager;



    protected override void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        base.Start();
        healthbar.SetMaxHealth(MaximumHP);
    }

    void Update()
    {
        if (currentHP <= 0)
        {
            Die();
        }
        foreach (Touch touch in Input.touches)
        {
            if (isCharmed == true || isSlept == true)
            {
                return;
            }
            if (touch.phase == TouchPhase.Moved)
            {
                playerAttack(touch);
            }
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            if (isCharmed == true|| isSlept == true)
            {
                return;
            }
            Animator.SetTrigger("Attack");
            Attack();
        }
        healthbar.SetHealth(currentHP);

    }



    void playerAttack(Touch touch)
    {
        Animator.SetTrigger("Attack");
        Attack();
        UnityEngine.Debug.Log("Attack");
    }


    public override void Die()
    {
        gameManager.Reset();
        Destroy(gameObject);
    }


    public void Charmed(bool charmCheck)
    {
        isCharmed = charmCheck;
        if (isCharmed == true)
        {
            GetComponent<PlayerMovement>().enabled = false;
        }
        else if (isCharmed == false)
        {
            GetComponent<PlayerMovement>().enabled = true;
        }

    }

    public void GotoUpgrade()
    {
        gameManager.GoToUpgradeScreen();
    }

}




