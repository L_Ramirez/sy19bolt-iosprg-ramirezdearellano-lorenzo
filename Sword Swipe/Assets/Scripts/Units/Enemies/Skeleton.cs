﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton : Enemy
{
    public Projectile projectile;

    protected override void Update()
    {
        base.Update();
        float distanceFromPlayer = Vector3.Distance(player.transform.position, this.transform.position);
        if (distanceFromPlayer > AttackRange && attackTimer >= 15)
        {
            if (Animator != null) Animator.SetTrigger("Attack");
            Instantiate(projectile, AttackArea.position, AttackArea.rotation);
            attackTimer = 0;
            UnityEngine.Debug.Log("Fire");
        }
    }
}
