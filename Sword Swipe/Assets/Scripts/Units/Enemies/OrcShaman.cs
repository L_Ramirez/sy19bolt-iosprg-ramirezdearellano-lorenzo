﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrcShaman : RangedEnemy
{
    public GameObject BuffTotem;
    public Transform TotemPlacement;
    private GameObject spawnedTotem;


    public override void Attack()
    {
        int attackChoice = UnityEngine.Random.Range(1, 100);
        if (attackChoice > 50 && spawnedTotem == null)
        {
            SummonTotem();
        }
        else if (attackChoice <= 50 || spawnedTotem !=null)
        {
         base.Attack();
        }
    }



    public void SummonTotem()
    {
        spawnedTotem = Instantiate(BuffTotem) as GameObject;
        spawnedTotem.transform.position = TotemPlacement.transform.position;
    }
}
