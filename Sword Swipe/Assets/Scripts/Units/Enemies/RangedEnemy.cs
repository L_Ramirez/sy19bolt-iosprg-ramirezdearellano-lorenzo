﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : Enemy
{
    
    public GameObject projectile;


    public override void Attack()
    {
        if (Animator != null) Animator.SetTrigger("Attack");
        Instantiate(projectile, AttackArea.position, AttackArea.rotation);
        UnityEngine.Debug.Log("Fire");
    }
}
