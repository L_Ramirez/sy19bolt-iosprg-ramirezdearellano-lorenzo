﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class Enemy : UnitStats
{
    protected Player player;
    protected float attackTimer;
    public float AttackDelay;
    public int DropGold = 0;
    protected int visionRange = 10;
    private GameManager gameManager;

    protected override void Start()
    {
        base.Start();
        player = FindObjectOfType<Player>();
        gameManager = FindObjectOfType<GameManager>();

    }


    protected virtual void Update()
    {
        attackTimer += Time.deltaTime;
        if (currentHP <= 0)
        {
            Die();
        }

        Vector3 direction = Vector3.Normalize(player.transform.position - transform.position);

        float distanceFromPlayer = Vector3.Distance(player.transform.position, this.transform.position);
        if (distanceFromPlayer < AttackRange)
        {
            if (attackTimer >= AttackDelay)
            {
                Attack();
                attackTimer = 0;
            }
        }
        else if (distanceFromPlayer < visionRange && distanceFromPlayer > AttackRange )
        {
            MoveUnit(direction);
        }




    }



    public override void Die()
    {
        gameManager.PlayerDataInstance.PlayerGold += DropGold;
        Destroy(gameObject);
    }

 
    
}
