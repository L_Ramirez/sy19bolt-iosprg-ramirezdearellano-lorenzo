﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gargoyle : Enemy
{
    protected override void Update()
    {
        attackTimer += Time.deltaTime;
        if (currentHP <= 0)
        {
            Die();
        }

        Vector3 direction = Vector3.Normalize(player.transform.position - transform.position);

        float distanceFromPlayer = Vector3.Distance(player.transform.position, this.transform.position);
        if (distanceFromPlayer < AttackRange)
        {
            if (attackTimer >= AttackDelay)
            {
                Attack();
                attackTimer = 0;
            }
          

        }
        else if (distanceFromPlayer < visionRange && distanceFromPlayer > AttackRange)
        {
            if (attackTimer < AttackDelay)
            {
                direction *= -1;
            }
                MoveUnit(direction);
        }

    }



}
