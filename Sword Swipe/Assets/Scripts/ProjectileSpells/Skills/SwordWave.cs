﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordWave : Projectile
{
    private GameManager gameManager;
    private int DMG;
    private int UpgradeLevel = 1;

    protected override void Start()
    {
        base.Start();
        gameManager = FindObjectOfType<GameManager>();
        UpgradeLevel = gameManager.PlayerDataInstance.SwordWaveLevel;

    }

    void checkUpgrades()
    {
        DMG += 2 * UpgradeLevel;
    }

    protected override void OnTriggerEnter2D(Collider2D hitInfo)
    {
        UnityEngine.Debug.Log(hitInfo.name);
        UnitStats target = hitInfo.GetComponent<UnitStats>();
        if (target != null)
        {
            target.TakeDamage(Shooter.ATK + DMG);
            deleteProjectile();
        }
    }
}
