﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : Projectile
{
    private GameManager gameManager;
    private int DMG;
    private int upgradeLevel = 1;
    public GameObject Burn;

    protected override void Start()
    {
        base.Start();
        gameManager = FindObjectOfType<GameManager>();
        upgradeLevel = gameManager.PlayerDataInstance.FireBallLevel;

    }

    void checkUpgrades()
    {
        DMG += 5 * upgradeLevel;
    }


    protected override void OnTriggerEnter2D(Collider2D hitInfo)
    {
        UnityEngine.Debug.Log(hitInfo.name);
        UnitStats target = hitInfo.GetComponent<UnitStats>();
        if (target != null)
        {
            target.TakeDamage(DMG);
            if(upgradeLevel == 5)
            {
                Burn.GetComponent<DamageOverTime>().SetTarget(target);
                Instantiate(Burn, this.transform.position, this.transform.rotation);
            }
        }
        deleteProjectile();
    }
}
