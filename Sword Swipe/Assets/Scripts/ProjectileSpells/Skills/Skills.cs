﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skills : MonoBehaviour
{
    public Player Player;
    public float CoolDown;
    public float CoolDownTimer;
    public GameObject SkillObject;
    public int level;

    public virtual void ActivateSkill()
    {
        if (CoolDownTimer <= 0)
        {
            Instantiate(SkillObject, Player.AttackArea.position, Player.AttackArea.rotation);
            CoolDownTimer = CoolDown;
        }
        else
        {
            UnityEngine.Debug.Log("Skill is on CoolDown");
        }
    }

    void Update()
    {
        if (CoolDownTimer > 0)
        {
            CoolDownTimer -= 1 * Time.deltaTime;
        }
    }

}
