﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceWall : MonoBehaviour
{
    public float Duration;
    private GameManager gameManager;
    private int upgradeLevel = 1;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        upgradeLevel = gameManager.PlayerDataInstance.IceWallLevel;
        checkUpgrades();

    }
    // Update is called once per frame
    void Update()
    {
        Duration -= Time.deltaTime;

        if (Duration <= 0)
        {
            destroyWall();
        }
    }

    void checkUpgrades()
    {
        Duration += 1 * upgradeLevel;
    }




    private void destroyWall()
    {
        Destroy(this.gameObject);
    }
}
