﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado : MonoBehaviour
{
    public float Duration;
    private GameManager gameManager;
    private int upgradeLevel = 1;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        upgradeLevel = gameManager.PlayerDataInstance.TornadoLevel;
        checkUpgrades();
    }

    void checkUpgrades()
    {
        Duration += 1 * upgradeLevel;
    }

    // Update is called once per frame
    void Update()
    {
        Duration -= Time.deltaTime;

        if (Duration <= 0)
        {
            destroyWall();
        }
    }



    private void destroyWall()
    {
        Destroy(this.gameObject);
    }
}
