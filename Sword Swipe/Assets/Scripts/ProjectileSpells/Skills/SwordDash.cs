﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordDash : Skills
{
    public float distance;
    public override void ActivateSkill()
    {
        if (CoolDownTimer <= 0)
        {
            Player.transform.position += new Vector3(distance,0,0);
            CoolDownTimer += CoolDown;
        }
        else
        {
            UnityEngine.Debug.Log("Skill is on CoolDown");
        }
    }
}
