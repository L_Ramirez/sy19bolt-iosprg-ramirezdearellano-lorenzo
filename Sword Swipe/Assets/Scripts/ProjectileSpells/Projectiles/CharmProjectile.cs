﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharmProjectile : Projectile
{
    public GameObject Charm;


    protected override void Start()
    {
        base.Start();
    }

    protected override void OnTriggerEnter2D(Collider2D hitInfo)
    {

        if (hitInfo.gameObject.CompareTag("Player"))
        {
            Player target = hitInfo.GetComponent<Player>();
            Charm.GetComponent<TimedEffect>().SetTarget(target);
            Instantiate(Charm, this.transform.position, this.transform.rotation);
            deleteProjectile();
        }
    }
}
