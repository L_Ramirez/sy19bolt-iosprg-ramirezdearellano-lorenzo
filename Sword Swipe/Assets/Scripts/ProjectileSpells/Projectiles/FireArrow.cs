﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireArrow : Projectile
{
    public GameObject Burn;


    protected override void Start()
    {
        base.Start();
    }

    protected override void OnTriggerEnter2D(Collider2D hitInfo)
    {
        UnityEngine.Debug.Log(hitInfo.name);
        if (hitInfo.gameObject.CompareTag("Player"))
        {
            UnitStats target = hitInfo.GetComponent<UnitStats>();
            Burn.GetComponent<DamageOverTime>().SetTarget(target);
            Instantiate(Burn, this.transform.position, this.transform.rotation);
            target.TakeDamage(Shooter.ATK);

            deleteProjectile();
        }
    }
}
