﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SleepProjectile : Projectile
{
    public GameObject Sleep;


    protected override void Start()
    {
        base.Start();
    }

    protected override void OnTriggerEnter2D(Collider2D hitInfo)
    {

        if (hitInfo.gameObject.CompareTag("Player"))
        {
            Player target = hitInfo.GetComponent<Player>();
            Sleep.GetComponent<TimedEffect>().SetTarget(target);
            Instantiate(Sleep, this.transform.position, this.transform.rotation);
            deleteProjectile();
        }
    }
}
