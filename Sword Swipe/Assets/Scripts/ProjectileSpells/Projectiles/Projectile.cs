﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed = 20f;
    public Rigidbody2D rb;
    public UnitStats Shooter;


    protected virtual void Start()
    {
        rb.velocity = transform.right * speed;
        Invoke("deleteProjectile", 3.0f);
    }

    protected virtual void OnTriggerEnter2D(Collider2D hitInfo)
    {
        UnityEngine.Debug.Log(hitInfo.name);
        Player target = hitInfo.GetComponent<Player>();
        if (target != null)
        {
            target.TakeDamage(Shooter.ATK);
        }
        deleteProjectile();
    }

    protected void deleteProjectile()
    {
        Destroy(this.gameObject);
    }

}
