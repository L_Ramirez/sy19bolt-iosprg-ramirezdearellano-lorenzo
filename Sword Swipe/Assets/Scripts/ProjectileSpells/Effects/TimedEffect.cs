﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedEffect : MonoBehaviour
{
    public float Duration;
    public float StartTime;
    public float RepeatTime;
    public UnitStats target;
    public Player PlayerTarget;


    void Start()
    {
        if (RepeatTime > 0)
        {
            InvokeRepeating("ApplyEffect", StartTime, RepeatTime);
        }
        else
        {
            Invoke("ApplyEffect", StartTime);
        }

        Invoke("EndEffect", Duration);
    }


    public void SetTarget(UnitStats targetToSet)
    {
        target = targetToSet;
    }


    public void SetTarget(Player targetToSet)
    {
        PlayerTarget = targetToSet;
    }


    protected virtual void ApplyEffect()
    {

    }

    protected virtual void EndEffect()
    {
        CancelInvoke();
        Destroy(gameObject);
    }

}
