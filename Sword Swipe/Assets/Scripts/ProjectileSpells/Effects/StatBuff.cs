﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatBuff : TimedEffect
{
    public int ATKChange;
    public float SPDChange;
    public int DEFChange;


    protected override void ApplyEffect()
    {
        target.StatusChange(ATKChange, SPDChange, DEFChange);
    }

    protected override void EndEffect()
    {
        target.StatusChange(-ATKChange, -SPDChange, -DEFChange);
    }
}
