﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sleep : TimedEffect
{
    protected override void ApplyEffect()
    {
        PlayerTarget.Charmed(true);
    }

    protected override void EndEffect()
    {
        PlayerTarget.Charmed(false);
        CancelInvoke();
        Destroy(gameObject);
    }
}
