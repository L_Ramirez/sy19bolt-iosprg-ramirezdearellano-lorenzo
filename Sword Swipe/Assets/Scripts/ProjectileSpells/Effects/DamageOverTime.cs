﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOverTime : TimedEffect
{
    public int damage;

    protected override void ApplyEffect()
    {
        target.setBurning(true);
        target.TakeDamage(damage);

    }

    protected override void EndEffect()
    {
        target.setBurning(false);
        base.EndEffect();
    }
}
