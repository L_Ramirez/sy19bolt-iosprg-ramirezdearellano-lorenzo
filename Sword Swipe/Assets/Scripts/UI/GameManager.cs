﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PlayerData
{
    public int PlayerGold = 200;
    public int FireBallLevel = 1;
    public int SwordWaveLevel = 1;
    public int TornadoLevel = 1;
    public int IceWallLevel = 1;
    public int SwordDashLevel = 1;
    //public Skills SkillList[];
}

public class GameManager : MonoBehaviour
{
    public Text text;
    public PlayerData PlayerDataInstance;



    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        PlayerDataInstance = new PlayerData();
        //subscribe to the event/ If event occurs then function is done
        SceneManager.sceneLoaded += this.OnLoad;
        if (SceneManager.GetActiveScene().name == "StartingScreen")
        {
            Load();
            returntoLevel();
        }
    }

    void Update()
    {
        if (PlayerDataInstance.PlayerGold <= 0)
        {
            PlayerDataInstance.PlayerGold = 0;
        }
        text.text = PlayerDataInstance.PlayerGold.ToString();

        if (Input.GetKeyDown(KeyCode.L))
        {
            SceneManager.LoadScene("Upgrade Screen");
        }

        // DEBUG MODE
        if (Input.GetKeyDown(KeyCode.S))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Load();
        }
    }

    //variables are required and static
    void OnLoad(Scene scene, LoadSceneMode mode)
    {
        if (GameObject.FindGameObjectWithTag("Gold") != null)
        {
            text = GameObject.FindGameObjectWithTag("Gold").GetComponent<Text>();
        }
        
    }
    
    public void Save()
    {
        Debug.Log("Saving...");
        string json = JsonUtility.ToJson(PlayerDataInstance);
        Debug.Log(json);
        PlayerPrefs.SetString("saveFile", json);
    }

    public void Load()
    {
        Debug.Log("Loading...");
        if (PlayerPrefs.GetString("saveFile") != null)
        {
            string json = PlayerPrefs.GetString("saveFile");
            Debug.Log(json);
            PlayerDataInstance = JsonUtility.FromJson<PlayerData>(json);
        }
    }

    public void returntoLevel()
    {
        Save();
        SceneManager.LoadScene("SampleScene");
    }

    public void GoToUpgradeScreen()
    {
        SceneManager.LoadScene("Upgrade Screen");
    }
    
    public void Reset()
    {
        PlayerDataInstance.PlayerGold = 0;
        PlayerDataInstance.FireBallLevel = 1;
        PlayerDataInstance.SwordWaveLevel = 1;
        PlayerDataInstance.TornadoLevel = 1;
        PlayerDataInstance.IceWallLevel = 1;
        PlayerDataInstance.SwordDashLevel = 1;
}

}
