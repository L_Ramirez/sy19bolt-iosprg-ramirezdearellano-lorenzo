﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour
{
    private GameManager gameManager;
    public Text FireBallGoldReq;
    public Text SwordWaveGoldReq;
    public Text IceWallGoldReq;
    public Text TornadoGoldReq;
    public Text SwordDashGoldReq;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        FireBallGoldReq.text = (gameManager.PlayerDataInstance.FireBallLevel * 100).ToString();
        SwordWaveGoldReq.text = (gameManager.PlayerDataInstance.SwordWaveLevel * 100).ToString();
        IceWallGoldReq.text = (gameManager.PlayerDataInstance.IceWallLevel * 100).ToString();
        TornadoGoldReq.text = (gameManager.PlayerDataInstance.TornadoLevel * 100).ToString();
        SwordDashGoldReq.text = (gameManager.PlayerDataInstance.SwordDashLevel * 100).ToString();

    }

    public void loadLevel()
    {
        gameManager.returntoLevel();
    }

    //public void UpgradeSkill(string skillName)
    //{
    //   foreach(Skills a in gameManager.PlayerDataInstance.SkillList)
    //    {
    //        if (a.id == skillName)
    //        {
    //            int skillLevel = a.level;
    //            if (gameManager.PlayerDataInstance.PlayerGold < skillLevel * 100)
    //            {
    //                UnityEngine.Debug.Log("Not Enough PlayerGold");
    //                return;
    //            }
    //            else
    //            {
    //                gameManager.PlayerDataInstance.PlayerGold -= skillLevel * 100;
    //                a.level += 1;
    //                gameManager.PlayerDataInstance.FireBallLevel = skillLevel;
    //                FireBallGoldReq.text = (skillLevel * 100).ToString();
    //            }
    //        }
    //    }
    //}

    public void UpgradeFireBall()
    {
        int fireBallLevel = gameManager.PlayerDataInstance.FireBallLevel;
        if(gameManager.PlayerDataInstance.PlayerGold < fireBallLevel * 100)
        {
            UnityEngine.Debug.Log("Not Enough PlayerGold");
            return;
        }
        else
        {
            gameManager.PlayerDataInstance.PlayerGold -= fireBallLevel * 100;
            fireBallLevel += 1;
            gameManager.PlayerDataInstance.FireBallLevel = fireBallLevel;
            FireBallGoldReq.text = (gameManager.PlayerDataInstance.FireBallLevel * 100).ToString();
        }
    }

    public void UpgradeSwordWave()
    {
        int swordWaveLevel = gameManager.PlayerDataInstance.SwordWaveLevel;
        if (gameManager.PlayerDataInstance.PlayerGold < swordWaveLevel * 100)
        {
            UnityEngine.Debug.Log("Not Enough PlayerGold");
            return;
        }
        else
        {
            gameManager.PlayerDataInstance.PlayerGold -= swordWaveLevel * 100;
            swordWaveLevel += 1;
            gameManager.PlayerDataInstance.SwordWaveLevel = swordWaveLevel;
            SwordWaveGoldReq.text = (gameManager.PlayerDataInstance.SwordWaveLevel * 100).ToString();
        }
    }

    public void UpgradeIceWall()
    {
        int iceWallLevel = gameManager.PlayerDataInstance.IceWallLevel;
        if (gameManager.PlayerDataInstance.PlayerGold < iceWallLevel * 100)
        {
            UnityEngine.Debug.Log("Not Enough PlayerGold");
            return;
        }
        else
        {
            gameManager.PlayerDataInstance.PlayerGold -= iceWallLevel * 100;
            iceWallLevel += 1;
            gameManager.PlayerDataInstance.IceWallLevel = iceWallLevel;
            IceWallGoldReq.text = (gameManager.PlayerDataInstance.IceWallLevel * 100).ToString();
        }
    }

    public void UpgradeTornado()
    {
        int tornadoLevel = gameManager.PlayerDataInstance.TornadoLevel;
        if (gameManager.PlayerDataInstance.PlayerGold < tornadoLevel * 100)
        {
            UnityEngine.Debug.Log("Not Enough PlayerGold");
            return;
        }
        else
        {
            gameManager.PlayerDataInstance.PlayerGold -= tornadoLevel * 100;
            tornadoLevel += 1;
            gameManager.PlayerDataInstance.TornadoLevel = tornadoLevel;
            TornadoGoldReq.text = (gameManager.PlayerDataInstance.TornadoLevel * 100).ToString();
        }
    }

    public void UpgradeSwordDash()
    {
        int swordDashLevel = gameManager.PlayerDataInstance.SwordWaveLevel;
        if (gameManager.PlayerDataInstance.PlayerGold < swordDashLevel * 100)
        {
            UnityEngine.Debug.Log("Not Enough PlayerGold");
            return;
        }
        else
        {
            gameManager.PlayerDataInstance.PlayerGold -= swordDashLevel * 100;
            swordDashLevel += 1;
            gameManager.PlayerDataInstance.SwordDashLevel = swordDashLevel;
            SwordDashGoldReq.text = (gameManager.PlayerDataInstance.SwordDashLevel * 100).ToString();
        }
    }

}
