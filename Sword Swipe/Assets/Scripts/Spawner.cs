﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float enemySpawnTime;
    public GameObject[] enemyList;
    private float spawnTimer;
    public GameObject[] numberOfEnemies;
    public bool newSpawn = false;
    public int spawnLimit;



    // Start is called before the first frame update
    void Start()
    {
        spawnTimer = enemySpawnTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (newSpawn)
        {
            numberOfEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            newSpawn = false;
        }
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0 && numberOfEnemies.Length <= 10)
        {
            Spawn();
            spawnTimer = enemySpawnTime;
        }
    }
    private void Spawn()
    {
        Instantiate(enemyList[UnityEngine.Random.Range(0, enemyList.Length - 1)],this.transform.position,this.transform.rotation);
        newSpawn = true;
    }
}
