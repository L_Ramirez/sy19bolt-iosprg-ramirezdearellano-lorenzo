﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Totem : MonoBehaviour
{
    public int EffectArea;
    public GameObject StatBuff;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("delete", 3.0f);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(this.transform.position, EffectArea);
    }

    protected virtual void OnTriggerEnter2D(Collider2D hitInfo)
    {
        UnityEngine.Debug.Log(hitInfo.name);
        Enemy target = hitInfo.GetComponent<Enemy>();
        if (target != null)
        {
            StatBuff.GetComponent<TimedEffect>().SetTarget(target);
            Instantiate(StatBuff, this.transform.position, this.transform.rotation);
        }
    }


    protected void delete()
    {
        Destroy(this.gameObject);
    }

}
