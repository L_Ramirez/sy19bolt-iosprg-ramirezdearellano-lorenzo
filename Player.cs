using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class Player : MonoBehaviour

{
    public GameObject player;
    public float movementSpeed;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Stationary)
            {
                playerMovement(touch);
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                playerAttack(touch);
            }
        }

        //Rigidbody2D rb = GetComponent<Rigidbody2D>();
        //if (Input.GetKey(KeyCode.A))
        //    rb.AddForce(Vector3.left);
        //if (Input.GetKey(KeyCode.D))
        //    rb.AddForce(Vector3.right);
        //if (Input.GetKey(KeyCode.W))
        //    rb.AddForce(Vector3.up);

    }





    void playerMovement(Touch touch)
    {

        if (touch.position.x > 0)
        {
            this.transform.position += new Vector3(10, 0) * Time.deltaTime * movementSpeed;
            UnityEngine.Debug.Log("move right");
        }
        else if (touch.position.x < 0)
        {
            this.transform.position -= new Vector3(10, 0) * Time.deltaTime * movementSpeed;
            UnityEngine.Debug.Log("move left");
        }

    }

    void playerAttack(Touch touch)
    {
        UnityEngine.Debug.Log("Attack");
    }




}




